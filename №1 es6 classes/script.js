// Створити клас Employee, у якому будуть такі характеристики - name
// (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee,
// і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary.
// Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
  set name(newName) {
    if (newName && newName.length > 0) {
      this._name = newName;
    } else {
      console.error("Invalid age");
    }
  }
  set age(newAge) {
    if (newAge > 0) {
      this._age = newAge;
    } else {
      console.error("Invalid age");
    }
  }
  set salary(newSalary) {
    if (newSalary > 0) {
      this._salary = newSalary;
    } else {
      console.error("Invalid age");
    }
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get lang() {
    return this._lang;
  }
  set lang(newLang) {
    if (Array.isArray(newLang) && newLang.length > 0) {
      this._lang = newLang;
    } else {
      console.error("Invalid languages list");
    }
  }
  get salary() {
    return this._salary * 3;
  }
}
const programmer_1 = new Programmer("Oleh", 24, 3000, "JavaScript");
const programmer_2 = new Programmer("Anna", 26, 5000, "Java");
const programmer_3 = new Programmer("Alice", 18, 1000, "Python");
const programmer_4 = new Programmer("John", 42, 1500, "C#");
const programmer_5 = new Programmer("Conor", 35, 4200, "C++");
console.log(programmer_1);
console.log(programmer_2);
console.log(programmer_3);
console.log(programmer_4);
console.log(programmer_5);
