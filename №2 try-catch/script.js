// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
//  Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const wrapper = document.getElementById("root");
const ul = document.createElement("ul");
wrapper.append(ul);

function checkProperties(books) {
  if (books.author === undefined) {
    throw new Error(`property of author is not exist`);
  }
  if (books.name === undefined) {
    throw new Error(`property of name is not exist`);
  }
  if (books.price === undefined) {
    throw new Error(`property of price is not exist`);
  }
}

books.forEach((book) => {
  try {
    checkProperties(book);
    const li = document.createElement("li");
    li.textContent = `автор: ${book.author}, ім'я книги: "${book.name}", вартість ${book.price} `;
    ul.append(li);
  } catch (error) {
    console.log("Error:", error.message);
  }
});
