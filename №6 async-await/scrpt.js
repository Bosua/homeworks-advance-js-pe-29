// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const IP_URL = "https://api.ipify.org/?format=json";
const btnIp = document.getElementById("search-btn");
const dataContainer = document.getElementById("dataContainer");

async function getUserIP() {
  try {
    const response = await fetch(IP_URL);
    if (!response.ok) {
      throw new Error("Failed to fetch user's IP");
    }
    const userIP = await response.json();
    console.log(userIP.ip); // {ip: '31.43.49.137'}
    return userIP.ip;
  } catch (error) {
    console.error("Fetch error", error);
  }
}

async function getPhysicalAddress(ip) {
  try {
    const response = await fetch(
      `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`
    );
    if (!response.ok) {
      throw new Error("Failed to fetch user's physical address");
    }
    const PhysicalAddress = await response.json();
    return PhysicalAddress;
  } catch (error) {
    console.error("Fetch error", error);
  }
}

async function render() {
  const searchBtn = document.getElementById("search-btn");
  searchBtn.addEventListener("click", async () => {
    const ip = await getUserIP();
    if (ip) {
      const address = await getPhysicalAddress(ip);

      const { continent, country, regionName, city, district } = address;

      // Створення нових елементів для кожного поля
      const continentElement = document.createElement("p");
      continentElement.textContent = `Континент: ${continent}`;

      const countryElement = document.createElement("p");
      countryElement.textContent = `Країна: ${country}`;

      const regionElement = document.createElement("p");
      regionElement.textContent = `Регіон: ${regionName}`;

      const cityElement = document.createElement("p");
      cityElement.textContent = `Місто: ${city}`;

      const districtElement = document.createElement("p");
      districtElement.textContent = `Район: ${district}`;

      // Очищення контейнера перед додаванням нових даних
      dataContainer.innerHTML = "";

      // Додавання нових елементів у контейнер
      dataContainer.appendChild(continentElement);
      dataContainer.appendChild(countryElement);
      dataContainer.appendChild(regionElement);
      dataContainer.appendChild(cityElement);
      dataContainer.appendChild(districtElement);
    }
  });
}
render();
