// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу
//  https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

const users_URL = "https://ajax.test-danit.com/api/json/users";
const posts_URL = "https://ajax.test-danit.com/api/json/posts";

class Card {
  constructor(userName, post) {
    this.userName = userName;
    this.post = post;
    this.cardElement = null; // Зберігаємо посилання на елемент карточки
  }

  renderCard() {
    const cardWrapper = document.getElementById("cardWrapper");
    const templateCard = document.getElementById("templateCard");
    const templateCardContent = templateCard.content.cloneNode(true);

    // Заповнюємо карточку
    const name = templateCardContent.querySelector(".name");
    name.textContent = this.userName.name;

    const nickName = templateCardContent.getElementById("nickname");
    nickName.textContent = `@${this.userName.username}`;

    const postTitle = templateCardContent.querySelector("#postTitle");
    postTitle.textContent = this.post.title;

    const postText = templateCardContent.querySelector("#postText");
    postText.textContent = this.post.body;

    const avatar = templateCardContent.querySelector("#avatar");
    avatar.src = `https://picsum.photos/300/200?random=${this.userName.id}`;

    // Видаляємо карточку
    const deleteButton = templateCardContent.querySelector(".delete-btn");
    deleteButton.addEventListener("click", () => deleteCard(this.post.id, this.cardElement));

    // Додаємо карточку до DOM після налаштування обробника подій
    this.cardElement = templateCardContent.firstElementChild;
    cardWrapper.appendChild(this.cardElement);
  }
}

async function getUsersData() {
  try {
    const response = await fetch(users_URL);
    if (!response.ok) {
      throw new Error("Failed to fetch users data");
    }
    const users = await response.json();
    const posts = await getPostsData();

    users.forEach((user) => {
      const userPosts = posts.filter((post) => post.userId === user.id);
      userPosts.forEach((post) => {
        const card = new Card(user, post);
        card.renderCard();
      });
    });
  } catch (error) {
    console.error("Fetch error", error);
  }
}

async function getPostsData() {
  try {
    const response = await fetch(posts_URL);
    if (!response.ok) {
      throw new Error("Failed to fetch posts data");
    }
    const posts = await response.json();
    return posts;
  } catch (error) {
    console.error("Fetch error", error);
  }
}

function deleteCard(postId, cardElement) {
  fetch(`${posts_URL}/${postId}`, {
    method: "DELETE",
  })
    .then((res) => {
      if (res.status === 200) {
        if (cardElement) {
          console.log(cardElement);
          cardElement.remove();
        } else {
          throw new Error("the element was not found");
        }
      } else {
        throw new Error("Delite was failed");
      }
    })
    .catch((error) => console.error("Помилка видалення", error));
}
getUsersData();
