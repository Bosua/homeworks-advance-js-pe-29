// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

document.addEventListener("DOMContentLoaded", () => {
  const filmList_URL = "https://ajax.test-danit.com/api/swapi/films";

  fetch(filmList_URL)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to fetch films");
      }
      return response.json();
    })
    .then((films) => {
      console.log(films);
      films.sort((a, b) => a.episodeId - b.episodeId);
      const body = document.querySelector("body");
      const filmlist = document.createElement("ul");
      films.forEach((film) => {
        const filmItem = document.createElement("li");

        const filmName = document.createElement("h2");
        filmName.textContent = `Episoded: ${film.episodeId} "${film.name}"`;

        const filmCharacters = document.createElement("p");
        filmCharacters.textContent = "Loading characters...";

        const description = document.createElement("p");

        description.textContent = `Description: "${film.openingCrawl}"`;

        filmItem.append(filmName, filmCharacters, description);
        filmlist.append(filmItem);
        body.append(filmlist);

        Promise.all(
          film.characters.map((character_URL) => {
            return fetch(character_URL).then((res) => {
              if (!res.ok) {
                throw new Error("Failed to fetch character");
              }
              return res.json();
            });
          })
        )
          .then((characters) => {
            filmCharacters.textContent = `Characters: ${characters
              .map((character) => character.name)
              .join(", ")}`;
          })
          .catch((error) => {
            filmCharacters.textContent = "Failed to load characters";
            console.error("Fetch error", error);
          });
      });
    })
    .catch((error) => console.error("Fetch error", error));
});
